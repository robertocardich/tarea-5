﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestaurant
{
    class Cuchillo : Console
    {
        public enum Type
        {
            Cuchillo_Mondador,
            Cuchillo_Cebollero,
            Cuchillo_para_pan
        }
        public Type type;

        public Cuchillo(string name, Type type, float cantidad) : base(name, cantidad)
        {
            this.type = type;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestaurant
{
    class Cuchara : Console
    {
        public enum Type
        {
            Cucharon,
            Cuchara_Mediana,
            Cuchara_para_postres
        }
        public Type type;

        public Cuchara(string name, Type type, float cantidad) : base(name, cantidad)
        {
            this.type = type;
        }
    }
}

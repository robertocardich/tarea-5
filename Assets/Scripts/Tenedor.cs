﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyRestaurant
{
    class Tenedor : Console
    {
        public enum Type
        {
            Tenedor_principal,
            Tenedor_para_postres,
            Tenedor_para_niños
        }

        public Type type;

        public Tenedor(string name, Type type, float cantidad) : base(name, cantidad)
        {
            this.type = type;
        }
    }
}

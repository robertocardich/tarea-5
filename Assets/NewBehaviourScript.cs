﻿using UnityEngine;
using MyRestaurant;
public class NewBehaviourScript : MonoBehaviour
{
    // Start is called before the first frame update
    static MyRestaurant.Console[] consoles;
    static Higiene[] higiene;
    static float total;

    void Start()
    {
        bool loop = true;


        consoles = new Console[3];
        consoles[0] = new Cuchara("Cucharon", Cuchara.Type.Cucharon, 5);
        consoles[1] = new Cuchillo("Cuchillo para pan", Cuchillo.Type.Cuchillo_para_pan, 2);
        consoles[2] = new Tenedor("Tenedor principal", Tenedor.Type.Tenedor_principal, 12);


            Console.Clear();

            Loop();
            System.Console.WriteLine("Continue?   (Y/N)");
            next = System.Console.ReadLine();
            loop = (next.ToUpper() == "Y");
        

    }

     void Loop()
    {
        System.Console.WriteLine("Consoles");
        System.Console.WriteLine("==========");

        for (int i = 0; i < consoles.Length, i++)
            System.Console.WriteLine("\t" + (i + i) + " - " + consoles[i].name + " A " + consoles[i].cantidad);

        string str = System.Console.ReadLine();
        int option = int.Parse(str);
        if (option < 4)
            total += consoles[option - 1].cantidad;
        else
            System.Console.WriteLine("Total" + total);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
